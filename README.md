How to use this repo
=====================
1. Clone the repo
2. Do npm istall
3. Run the app with ionic serve --lab or ionic serve

This is an Ionic app which is written using AngularJs

Logic
=======

The game has several levels and each level has several rounds. Essentially there are four bulbs which blink one after other and the player is
supposed to memorize and reproduce the sequence in a specified time.

Level 1 Round 1:
----------------
Only one bulb blinks for 0.7 seconds and in next 3 seconds the player is supposed to reproduce the sequence. 
If he succeeds then round 2 will start, otherwise same round. 

Level 1 Round 2:
----------------
One bulb binks for 0.7 sec and then other for 0.7 sec and in next 3 seconds the player is supposed to reproduce the sequence. 
If he succeeds then round 3 will start, otherwise same round. Level 1 has total 5 rounds and in each round one bulb will be added to the sequence. 
On successful completion of the level 1 - level 2 will start.

Level 2 Round 1:
----------------
Level 2 is similar to level 1, the only difference is that instead of 0.7 sec bulb will blink for 0.5 sec. This level has 7 rounds.
On successful completion of the level 2 - level 3 will start.

Level 3 Round 1:
----------------
Level 3 round 1 starts with sequence of 4 each blinking for 0.5 sec and then there are total 3 rounds. Each round adds 2 bulbs to the sequence. 

Level 4, 5, 6:
----------------

These are different than previous levels. Logic is not yet clear, but the bulb will be replaced with an image.  



