angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $timeout) {

  $scope.blinkingBoxes=[]
  $scope.clickedBoxes=[]
  
  $scope.level=1
  $scope.round=1
  $scope.count=1

  $scope.promptRound=''
  $scope.promptAttention=''
  $scope.promptAction=''
  $scope.promptCongratulations=''
  $scope.promptFailure=''
  
  $scope.disableStart='false'
  

  $scope.boxes = [{
    id:1,
    isLit: false,
    color: 'green'
  }, {
    id:2,
    isLit: false,
    color: 'blue'
  }, {
    id:3,
    isLit: false,
    color: 'red'
  }, {
    id:4,
    isLit: false,
    color: 'yellow'
  }];

  $scope.randomOrder = [];

  $scope.boxClick = function(boxId){
    $scope.clickedBoxes.push(boxId)
  }

  $scope.checkCorrectness= function(){
    if(angular.equals($scope.blinkingBoxes, $scope.clickedBoxes)){
      $scope.promptAction=''
      $scope.promptRound=''
      $scope.promptCongratulations='Congratulations! Click start to play next round.'
      $scope.round++
      $scope.count++
      if($scope.level==1 && $scope.round==5 || $scope.level==2 && $scope.round==7){
        $scope.level++
        $scope.round=1
        $scope.count=1
      }
      console.log("Level: " + $scope.level + " " + "Round: " + $scope.round + " " + "Count: " + $scope.count)
    }
    else{
      $scope.promptAction=''
      $scope.promptFailure='Wrong! Try again!'
    }
  }

  $scope.enterSequence = function(){

    var triggerCheckCorrectness
    switch($scope.round){
      case 1,2,3:
        triggerCheckCorrectness=3000;
        break;
      case 4,5,6:
        triggerCheckCorrectness=5000;
        break;
      default:
         triggerCheckCorrectness=3000;  
    }
   console.log("After haow many milisec to check correctness " + triggerCheckCorrectness) 
   $timeout($scope.checkCorrectness, triggerCheckCorrectness)

   $scope.promptAttention=''
   $scope.promptAction="Kindly enter the sequence"

      //Start the  3 seconds counter
      $scope.counter=3
      $scope.onTimeout = function(){
        $scope.counter--;
        mytimeout = $timeout($scope.onTimeout,1000);
        
        if(angular.equals($scope.clickedBoxes, $scope.blinkingBoxes) || $scope.counter==0){
          $timeout.cancel(mytimeout);
        }
      }
      var mytimeout = $timeout($scope.onTimeout,1000);
    }

    $scope.start = function() {

     var whenTriggerEntering;

     console.log("Start: " + $scope.level + " " + $scope.round)
     if($scope.level && $scope.round) {
      if($scope.round==1|| $scope.round==2){
        var whenTriggerEntering = 3000;  
      }
      else if($scope.round==3 || $scope.round==4 || $scope.round==5 || $scope.round==6){
        var whenTriggerEntering = 5000; 
      }
      $scope.promptRound = ['Level', $scope.level, 'Round', $scope.round].join(' ');
    } else {
      $scope.promptRound = 'Default'; 
    }

    $scope.promptFailure=''
    $scope.counter=0
    $scope.promptCongratulations=''
    $scope.blinkingBoxes=[]
    $scope.clickedBoxes=[]
    $scope.promptAttention='Observe the sequence'
    $scope.randomOrder = [];
    for (var i = 0; i < $scope.count; i++) {
      var randomNumber = Math.floor(Math.random() * 4);
      $scope.randomOrder.push(randomNumber);
    }
    $timeout(function() {
      $scope.blink(0);
    }, 1000);
    $timeout($scope.enterSequence, whenTriggerEntering)

  }

  $scope.blink = function(index) {

    var milisec;
    switch($scope.level){
      case 1: 
      milisec=500;
      break;
      case 2: 
      milisec=300;
      break;
      case 3: 
      milisec=300;
      break;
      default:
      milisec=200;
    }

    if (index < $scope.count) {
      $scope.boxes[$scope.randomOrder[index]].isLit = true;
      $scope.blinkingBoxes.push($scope.boxes[$scope.randomOrder[index]].id)
      $timeout(function() {
        $scope.boxes[$scope.randomOrder[index]].isLit = false;
        $timeout(function() {
          $scope.blink(index + 1);
        }, milisec);
      }, milisec);

    }
  }
});


